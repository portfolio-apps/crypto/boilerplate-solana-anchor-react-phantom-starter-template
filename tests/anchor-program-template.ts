import * as anchor from "@project-serum/anchor";
import { Program } from "@project-serum/anchor";
import { AnchorProgramTemplate } from "../target/types/anchor_program_template";

describe("anchor-program-template", () => {
  // Configure the client to use the local cluster.
  anchor.setProvider(anchor.AnchorProvider.env());

  const program = anchor.workspace.AnchorProgramTemplate as Program<AnchorProgramTemplate>;

  it("Is initialized!", async () => {
    // Add your test here.
    const tx = await program.methods.initialize().rpc();
    console.log("Your transaction signature", tx);
  });
});
