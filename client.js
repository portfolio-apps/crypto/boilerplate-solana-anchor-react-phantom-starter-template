const anchor = require("@project-serum/anchor");

const PROGRAM_ID = 'EqvtZptRvas1pUSukYXUywnnZoE66WS7mArbiWfdMfta';
const FILENAME = 'anchor_program_template';
const provider = anchor.AnchorProvider.local();

// Configure the local cluster.
anchor.setProvider(provider);

async function main() {
  // #region main
  // Read the generated IDL.
  const idl = JSON.parse(
    require("fs").readFileSync(`./target/idl/${FILENAME}.json`, "utf8")
  );

  // Address of the deployed program.
  try {
    const programId = new anchor.web3.PublicKey(PROGRAM_ID);

    // Generate the program client from IDL.
    const program = new anchor.Program(idl, programId);
  
    // Execute the RPC.
    const tx = await program.methods.initialize().rpc();
    console.log(`https://explorer.solana.com/tx/${tx}?cluster=custom&customUrl=http://localhost:8899`);
  } catch (e) {
    console.log(e)
  }
  // #endregion main
}

console.log("View Logs at link below, if failure make sure 'solana-test-validator' is running");
main().then(() => console.log("Success"));
