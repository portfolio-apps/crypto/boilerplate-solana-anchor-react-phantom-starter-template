# Phantom React Solana Start Template

### **Welcome 👋**
To get started with this demo, clone this repo and follow these commands:

1. Run `npm install` at the root of your directory
2. Run `npm run start` to start the project
3. Start coding!