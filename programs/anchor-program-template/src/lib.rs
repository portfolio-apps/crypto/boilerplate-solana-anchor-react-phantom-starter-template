use anchor_lang::prelude::*;

declare_id!("EqvtZptRvas1pUSukYXUywnnZoE66WS7mArbiWfdMfta");

#[program]
pub mod anchor_program_template {
    use super::*;

    pub fn initialize(_ctx: Context<Initialize>) -> Result<()> {
        let variable = 234324;
        msg!("Printing message from the initialze function, and printing variable {}", variable);
        Ok(())
    }
}

#[derive(Accounts)]
pub struct Initialize {}
