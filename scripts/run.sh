#!/bin/sh

$PUBKEY=$(solana address)

echo "------------------------------------------------------------------------"
echo ">> Airdropping SOL to address..."
echo "------------------------------------------------------------------------"
solana airdrop 2 $PUBKEY
echo ""
echo ""
echo "------------------------------------------------------------------------"
echo ">> Building & Deploying to network..."
echo "------------------------------------------------------------------------"
anchor build
anchor deploy
echo ""
echo ""
echo "------------------------------------------------------------------------"
echo ">> Initiating transaction..."
echo "------------------------------------------------------------------------"
ANCHOR_WALLET=./keypair/id.json node client.js