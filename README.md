# Solana Anchor React Phantom Starter Template

### **Welcome 👋**
To get started with this demo, clone this repo and follow these commands:

1. Run `solana-test-validator` to run test validator
2. Run `bash scripts/set_keypair.sh` to configure default keypair to one located in this folder
3. Run `bash scripts/set_localnet.sh` to configure the set keypair to the test validator
3. Run `bash scripts/run.sh` build and run the local program
4. Start coding!

### If You Make Changes To The Program
You'll need to update its Program ID, use the commands below to update where necessary

1. Run `bash scripts/files_where_id.sh <CURRENT_PROGRAM_ID>`
2. Change the ID in all files where the current shows (Can ignore test-ledger/logs).
    - In Anchor.toml
    - In the program `declare_id!()`
    - In the client.js for variable PROGRAM_ID
3. Run `bash scripts/run.sh` build and run the local program